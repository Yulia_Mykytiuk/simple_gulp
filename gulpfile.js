'use strict'
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    sourceMaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    rimraf = require('rimraf'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

var path = {
    build: {
        html: 'build/',
        js: 'build/javascript/',
        css: 'build/css/'
    },
    src: {
        html: 'src/*.html',
        js: 'src/javascript/*.js',
        css: 'src/css/*.{scss,css}'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/javascript/**/*.js',
        css: 'src/css/**/*.scss'
    },
    clean: './build'
};

gulp.task("webserver", function () {
   browserSync({
       server: {
           baseDir: "./build"
       },
       host: 'localhost',
       port: 3000,
       tunnel: true
   });
});

gulp.task("html:build", function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task("js:build", function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(sourceMaps.init())
        .pipe(sourceMaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});


gulp.task("style:build", function () {
    gulp.src(path.src.css)
        .pipe(sourceMaps.init())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourceMaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task("build", [
    'html:build',
    'js:build',
    'style:build'
]);

gulp.task("watch", function () {
   watch([path.watch.js], function (ev, callback) {
       gulp.start('js:build');
   });
    watch([path.watch.html], function (ev, callback) {
        gulp.start('html:build');
    });
    watch([path.watch.css], function (ev, callback) {
        gulp.start('style:build');
    });
});

gulp.task("clean", function (callback) {
    rimraf(path.clean, callback);
});

gulp.task('default', ['build', 'webserver', 'watch']);